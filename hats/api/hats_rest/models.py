from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()


class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100, null=True)
    color = models.CharField(max_length=100, null=True)
    url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    # # __str__ method creates a reference to the object
    # # it is designed to be a "human-readable string"
    def __str__(self):
        return self.style_name

    # # this creates the href for the object, IAW the ModelEncoder in common
    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"id": self.id})
