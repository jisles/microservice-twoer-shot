from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Hat, LocationVO
# from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name", "section_number", "shelf_number"]

    # def get_closet_location(self, instance):
    #     return instance.location.__str__()


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style_name",
        "fabric",
        "color",
        "url",
        ]

    def get_extra_data(self, o):
        return {"location": o.location.import_href}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style_name",
        "fabric",
        "color",
        "url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
          {"hats": hats},
          encoder=HatListEncoder,
        )
    else:
        new_hat = json.loads(request.body)
        try:
            import_href = new_hat["location"]
            location = LocationVO.objects.get(import_href=f"/api/locations/{import_href}/")
            new_hat["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400
            )
        hat = Hat.objects.create(**new_hat)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid hat id"},
                status=400,
                )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        new_hat = json.loads(request.body)
        Hat.objects.filter(id=id).update(**new_hat)
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
