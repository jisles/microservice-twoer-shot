import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatList from './HatList'
import HatCreate from './HatCreate'
import Nav from './Nav';

// props --> ref index.js line 19 "hats" ...
// can be inserted as 'unpacked' {hats} if prop name known
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList />} />
            <Route path="new" element={<HatCreate />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
