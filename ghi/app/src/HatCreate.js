import { useState, useEffect } from "react";

function HatCreate(){
  const [styleName, setStyleName] = useState('');
  const [fabric, setFabric] = useState('');
  const [color, setColor] = useState('');
  const [url, setUrl] = useState('');
  const [location, setLocation] = useState('');
  const [locations, setLocations] = useState([]);
  // const [hasSubmittedHat, setHasSubmittedHat] = useState(false);


  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.location = location
    data.style_name = styleName;
    data.color = color;
    data.url = url;
    data.fabric = fabric;
    console.log(`first log statement: ${JSON.stringify(data)}`)

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const hatResponse = await fetch(hatUrl, fetchOptions);
    if (hatResponse.ok) {
      setLocation('');
      setStyleName('');
      setColor('');
      setUrl('');
      setFabric('');
      setHasSubmittedHat(true);
    } else {
      console.log(`hatResponse error: ${JSON.stringify(hatResponse)}`)
    }
  }

  const handleChangeLocation = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const handleChangeStyleName = (event) => {
    const value = event.target.value;
    setStyleName(value);
  }

  const handleChangeColor = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const handleChangeUrl = (event) => {
    const value = event.target.value;
    setUrl(value);
  }

  const handleChangeFabric = (event) => {
    const value = event.target.value;
    setFabric(value);
  }

  const [hasSubmittedHat, setHasSubmittedHat] = useState(false);

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (locations.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (hasSubmittedHat) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-hat-form">
                <h1 className="card-title">Organize your hats, ye slob</h1>
                <p className="mb-3">
                  Select a hat storage location.
                </p>
                <div className={spinnerClasses} id="loading-location-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleChangeLocation} name="location" id="location" className={dropdownClasses} required>
                    <option value="">Choose a hat location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.id} value={location.id}>{location.closet_name} - {location.section_number} / {location.shelf_number}</option>
                      )
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about your hat.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeStyleName} required placeholder="Your hat name" type="text" id="style_name" name="style_name" className="form-control" />
                      <label htmlFor="name">Your hat name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeColor} required placeholder="Your hat color" type="text" id="color" name="color" className="form-control" />
                      <label htmlFor="color">Your hat color</label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeUrl} required placeholder="Your hat url" type="url" id="url" name="url" className="form-control" />
                      <label htmlFor="name">Your hat url</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeFabric} required placeholder="Your hat fabric" type="text" id="fabric" name="fabric" className="form-control" />
                      <label htmlFor="color">Your hat fabric</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Put it away, you fool</button>
              </form>
              <div className={messageClasses} id="success-message">
                Success... barely.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HatCreate
