import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// async function loadHats() {
//   const hatResponse = await fetch('http://localhost:8090/api/hats');
//   if (hatResponse.ok) {
//     const hatData = await hatResponse.json();

//     root.render(
//       <React.StrictMode>
//         <App hats={hatData.hats} />
//         {/* ref line 6 app.js */}
//       </React.StrictMode>
//     );
//   }  else {
//     console.error(hatResponse);
//   }
// }
// loadHats()
